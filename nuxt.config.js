import colors from "vuetify/es5/util/colors";

export default {
   mode: 'universal',
  // mode: 'spa',
  /*
  ** Headers of the page
  */
  server: {
    port: 3004, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  buildDir: 'nuxt-dist',
  router: {
    base: '/',
    routeNameSplitter: '/'
  },
  head: {
    titleTemplate: "%s",
    // titleTemplate: '%s - ' + process.env.npm_package_name,
    title: "Best Preschool Franchise in India | No Royalty Fee",
    // title: process.env.npm_package_name || '',
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content:
          "No royalty preschool franchise in India. We offer franchise options for play school, after-school and teacher training throughout India and abroad."
      },
      {
        hid: "keywords",
        name: "keywords",
        content: "preschool franchise, preschool franchise without royalty"
      }
      // { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Poppins&display=swap"
      },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/icon?family=Material+Icons"
      }
    ],
    // script: []
  },
  generate: {
    subFolders: true
  },
  router: {
    base: "/",
    routeNameSpilitter:'/'
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },
  /*
   ** Global CSS
   */
  css: ["~/assets/scss/main.scss", "plyr/dist/plyr.css"],
  /*
   ** Plugins to load before mounting the App
   */
  // plugins: ["~/plugins/vue-player", { src: "~/plugins/aos", ssr: false }],
  plugins: ["~/plugins/vue-player"],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ["@nuxtjs/vuetify"],
  /*
   ** Nuxt.js modules
   */
  modules: ["@nuxtjs/axios"],
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: "#f56912"
        }
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
};
