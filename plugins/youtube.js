import Vue from 'vue'
import LazyYoutubeVideo, {
  Plugin,
} from 'vue-lazy-youtube-video/dist/vue-lazy-youtube-video.ssr.common'
// have to import ejected style directly, see this issue https://github.com/vuejs/rollup-plugin-vue/issues/266
import 'vue-lazy-youtube-video/dist/style.css'

// as a global component
Vue.component('LazyYoutubeVideo', LazyYoutubeVideo)
